// 3. Find pairs of elements whose sum is equal to a given number
// Example: [1,7,9,2,5,3,8], sum = 10 => [1,9] ; [7,3]; [2,8]

function getSumEle(arr1, sum){
    let arr = [...new Set(arr1)]
    let eleArr = []
    for(let i=0; i<arr.length; i++){
        for(j=i+1; j<arr.length; j++){
            if(arr[i]+arr[j] == sum)
            eleArr.push([arr[i],arr[j]])
        }
    }
    return eleArr; 
}