// 5. Get a new array of duplicate elements in 2 arrays, each element appears exactly once in the new array
// Example: 	const arr1 = [1, 2, 3, 4, 5, 6, 7, 9, 9, 8, 7, 7];
// 		const arr2 = [3, 5, 9, 10, 88];
// 			=> [3,5,9]

function getSameEle(ar1, arr2){
    let sameEle = []
    for(let i=0; i<arr1.length; i++){
        if(arr2.includes(arr1[i]) && !sameEle.includes(arr1[i]))
            sameEle.push(arr1[i])
    }
    return sameEle
}