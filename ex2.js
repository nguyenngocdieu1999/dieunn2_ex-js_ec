// 2. Find 2 elements with the greatest sum in an array
// Example: [1,7,9,2,5,3,8] => [9.8]

function getSum(arr1){
    let sortArr = arr1.sort((a,b) => b-a)
    return [sortArr[0], sortArr[1]]
}