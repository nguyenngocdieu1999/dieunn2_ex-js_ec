// 1. Write a function with 2 input parameters (each parameter is an array). This function returns an array of the same elements in two arrays.

// Example: 	array1 = [1,2,3,4,5,6];
// 		array2 = [3,4,8,9,12];
// 		=> [3,4]

function getSameEle(ar1, arr2){
    let sameEle = []
    for(let i=0; i<arr1.length; i++){
        if(arr2.includes(arr1[i]))
            sameEle.push(arr1[i])
    }
    return sameEle
}