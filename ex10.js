//10. Write 3 methods myMap, myFilter, myReduce (suggest: user Array.prototype, "this" keyword, and for loop). 
//3 these methods have similar in use and usage as map, filter, reduce of array in JS

Array.prototype.myMap = function(callback) {
   let newArray = [];
   for(let i=0; i<this.length; i++){
       newArray.push(callback(this[i], i, this));
   }
   return newArray;
};

Array.prototype.myFilter = function(callback) {
   let newArray = [];
   for(let i=0; i<this.length; i++){
       if(callback(this[i], i, this) == true) 
        newArray.push(this[i]);
   }
   return newArray;
};

Array.prototype.myReduce = function(callback, defaultValue) {
    let total = defaultValue || this[0];
    for(let i=0; i<this.length; i++){
       total = callback(total, this[i], i, this)
   }
   return total;
};

